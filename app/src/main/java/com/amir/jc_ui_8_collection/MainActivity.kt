package com.amir.jc_ui_8_collection

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.amir.jc_ui_8_collection.ui.data.MockData
import com.amir.jc_ui_8_collection.ui.models.Shoes
import com.amir.jc_ui_8_collection.ui.theme.AppDark
import com.amir.jc_ui_8_collection.ui.theme.AppLight
import com.amir.jc_ui_8_collection.ui.theme.AppRed
import com.amir.jc_ui_8_collection.ui.theme.Jc_ui_8_collectionTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Jc_ui_8_collectionTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainView()
                }
            }
        }
    }
}

@Composable
private fun MainView() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(AppDark)
    ) {
        Column(Modifier.fillMaxSize()) {
            //header icon
            TopHeader()
            //title
            Text(
                text = "Collections",
                color = AppLight,
                fontSize = 28.sp,
                textAlign = TextAlign.Left,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(25.dp)
            )
            //body
            LazyVerticalGrid(
                columns = GridCells.Fixed(2),
                contentPadding = PaddingValues(10.dp)
            ) {
                items(MockData.list.size) {
                    val item = MockData.list[it]
                    var isLiked by remember { mutableStateOf(item.liked) }
                    BodyShoesItemsView(item, isLiked)
                }
            }
        }
    }
}

@Composable
private fun BodyShoesItemsView(item: Shoes, isLiked: Boolean) {
    var isLiked1 = isLiked
    Card(
        modifier = Modifier.fillMaxSize(),
        backgroundColor = Color.Transparent,
        elevation = 0.dp,
        shape = RoundedCornerShape(25.dp)
    ) {
        Column {
            Box(
                Modifier
                    .fillMaxWidth()
                    .height(230.dp)
            ) {
                Box(
                    Modifier
                        .fillMaxWidth()
                        .height(170.dp)
                        .padding(15.dp)
                        .clip(RoundedCornerShape(25.dp))
                        .align(Alignment.BottomCenter)
                ) {
                    Box(
                        Modifier
                            .fillMaxSize()
                            .background(AppLight)
                            .padding(15.dp)
                    ) {
                        Row(
                            Modifier
                                .fillMaxWidth()
                                .align(Alignment.BottomCenter),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.SpaceBetween
                        ) {
                            //card footer
                            CardFooterView(item, isLiked1)
                        }
                    }
                }
                //card shoes
                CardShoesView(item)
            }
            //price and model
            Text(
                text = item.name,
                color = Color.White,
                fontSize = 16.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth()
            )
            Text(
                text = "${item.price}$",
                color = AppLight,
                fontSize = 14.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth()
            )
        }
    }
}

@Composable
private fun CardFooterView(item: Shoes, isLiked1: Boolean) {
    var isLiked11 = isLiked1
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        //rate icon
        Icon(
            painter = painterResource(id = R.drawable.star),
            contentDescription = "Rate",
            tint = Color.White,
            modifier = Modifier.size(15.dp)
        )
        Spacer(modifier = Modifier.width(5.dp))
        Text(text = item.rate.toString(), fontSize = 12.sp)
    }
    //favorite icon
    IconButton(
        onClick = { isLiked11 = !isLiked11 },
        Modifier.size(28.dp)
    ) {
        Icon(
            painter =
            if (isLiked11) painterResource(id = R.drawable.heart_fill) else
                painterResource(id = R.drawable.heart_empty),
            contentDescription = "",
            Modifier
                .size(40.dp)
                .padding(5.dp)
                .clip(RoundedCornerShape(5.dp)),
            tint = if (isLiked11) AppRed else Color.White
        )
    }
}

@Composable
private fun CardShoesView(item: Shoes) {
    Box(
        Modifier
            .fillMaxWidth()
            .height(160.dp)
    ) {
        //to make shadow behind of the shoes images
        Box(
            Modifier
                .width(70.dp)
                .height(40.dp)
                .align(Alignment.BottomCenter)
                .shadow(15.dp, CircleShape)
        )
        Image(
            painter = painterResource(id = item.img),
            contentDescription = "IMG",
            modifier = Modifier
                .width(160.dp)
                .rotate(45f)
                .align(Alignment.TopCenter)
                .padding(top = 65.dp, start = 25.dp)
        )
    }
}

@Composable
fun TopHeader() {
    Row(
        Modifier
            .fillMaxWidth()
            .padding(25.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Icon(
            painter = painterResource(id = R.drawable.logo),
            contentDescription = "Logo",
            Modifier.size(40.dp),
            tint = Color.White
        )
        Icon(
            painter = painterResource(id = R.drawable.menu_dots),
            contentDescription = "Menu",
            Modifier.size(30.dp),
            tint = Color.White
        )
    }
}


@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    Jc_ui_8_collectionTheme {
        MainView()
    }
}