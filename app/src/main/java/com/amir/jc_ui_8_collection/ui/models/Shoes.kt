package com.amir.jc_ui_8_collection.ui.models

import androidx.annotation.DrawableRes

data class Shoes(
    var name: String,
    var price: Int,
    @DrawableRes var img: Int,
    var rate: Float,
    var liked: Boolean
)
